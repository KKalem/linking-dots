(ns linking-dots.core
  (:gen-class)
  (:require
   [clojure.pprint :refer [pprint]]
   [linking-dots.util :refer [log]]
   [linking-dots.minedge :refer [max-min-edge logging-grid]]))

(defn do-minedge [[ktrans ktotal path]]
  (max-min-edge (read-string ktrans) (read-string ktotal) path))

(defn do-grid [[ktrans-from ktrans-to ktotal-from ktotal-to step path-from path-to]]
  (logging-grid (read-string ktrans-from)
                (read-string ktrans-to)
                (read-string ktotal-from)
                (read-string ktotal-to)
                (read-string step)
                path-from
                path-to))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (cond
   (= "-grid" (first args)) (println "Grid is being saved to:" (last args) (do-grid (rest args)))
    (= "-minedge" (first args)) (println "Minedge: " (do-minedge (rest args)))
    :default (println "Illegal arguements entered, legal options: -minedge ktrans ktotal path or -grid ktrans-from ktrans-to ktotal-from ktotal-to step path-from path-to")))
