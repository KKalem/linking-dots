(ns linking-dots.di-graph
  (:gen-class)
  (:require
   [clojure.pprint :refer [pprint]]
   [linking-dots.util :as util]
   [linking-dots.lpfying :refer [chain]]
   [ubergraph.core :as uber]
   [ubergraph.alg :as alg]))

(defn normalize-attr
  "returns a list of given attributes of given word in the chain, normalized and in same order"
  [sub-chain kw]
  (->>
   sub-chain
   vals
   (map kw)
   util/normalize))

(defn inverse-normalize-attr
  "returns 1-normalized"
  [sub-chain kw]
  (map #(- 1 %) (normalize-attr sub-chain kw)))

(defn word-doc-edges
  "returns a list of edges FROM words TO docs. each edge is like [word doc normal-kw]"
  [chain word kw]
  (let [wd (inverse-normalize-attr (get-in chain [:word-chain :words word :docs])
                                   kw)
        ids (get-in chain [:word-chain :ids])
        filtered-ids (filter #(not= 0 (kw %)) ids)]
    (->>
     (zipmap ids wd)
     sort
     (map #(conj (reverse (into '() %)) word)))))

(defn doc-word-edges
  "returns a list of edges FROM docs TO words in it. each edge is like [doc word normal-kw]"
  [chain doc-id kw]
  (let [dw (inverse-normalize-attr (get-in chain [:final-chain doc-id])
                                   kw)
        words (keys (get-in chain [:final-chain doc-id]))
        filtered-w (filter #(not= 0 (kw %)) words)]
    (->>
     (zipmap filtered-w dw)
     (map #(conj (reverse (into '() %)) doc-id)))))

(defn add-all-edges-from-chain
  "returns a graph with all edges added from the given chain"
  [graph chain kw]
  (->
   graph
   (uber/add-edges* (mapcat #(word-doc-edges chain % kw) (->
                                                          chain
                                                          :word-chain
                                                          :words
                                                          keys)))
   (uber/add-edges* (mapcat #(doc-word-edges chain % kw) (->
                                                          chain
                                                          :word-chain
                                                          :ids)))))


;; ==================================================================================================================

(defn graph
  "returns the bi-partite graph of words-documents from the given chain. weights of edges are determined by kw"
  [chain kw]
  (add-all-edges-from-chain (uber/multidigraph) chain kw))

(defn influence
  "returns the influence value of a word over a document.
  word   -> string
  doc-id -> string
  g -> the bi-partite graph of the document chain"
  [word doc-id doc-id-next g]
  (- 1
     (*
      (-> (alg/shortest-path g {:start-node doc-id
                                :end-node word
                                :traverse false
                                :cost-attr :weight})
          alg/edges-in-path
          (->>
           (map #(uber/attr g % :weight))
           (reduce *)))
      (-> (alg/shortest-path g {:start-node word
                                :end-node doc-id-next
                                :traverse false
                                :cost-attr :weight})
          alg/edges-in-path
          (->>
           (map #(uber/attr g % :weight))
           (reduce *))))))

;; (def g (graph (chain "resources") :freq))

;; (uber/pprint g)
;; (influence "including" "1_0991630" "2_1040010" g)
