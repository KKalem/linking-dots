(ns linking-dots.lpfying
  (:gen-class)
  (:require [clojure.pprint :refer [pprint]]
            [linking-dots.word-set :refer [make-word-chain]]
            [linking-dots.docs :refer [doc-chain-from-dir]]
            [linking-dots.util :refer [log sum-to]]))


(def ^:dynamic *kTotal* 0.3)
(def ^:dynamic *kTrans* 0.2)
(def ^:dynamic *max-pairs* 200)


;; A(x) = f(x+1) - f(x)
;; I(x) = A(x+1) - A(x) = f(x+2) - 2f(x+1) - f(x)

(defn- raw-act [current-f next-f] (- next-f current-f))
(defn- raw-init [current-act next-act] (- next-act current-act))


(defn- act-value
  "Returns the act value for the given freqs"
  [current-f next-f]
  (let [act (raw-act current-f next-f)]
    (if (< act 0) 0 act)))

(defn- init-value
  "Returns the init value for the given freqs"
  [current-f next-f next-next-f]
  (let [init  (raw-init (act-value next-f next-next-f) (act-value current-f next-f))]
    (if (< init 0) 0 init)))


(defn- doc-freq-of-word
  "Returns the freq value of given doc-id for the word"
  [doc-map doc-id]
  (get (get doc-map doc-id) :freq 0))

(defn value-map
  "returns a map {:freq freq :act act :init init"
  [doc-map current-doc-id next-doc-id next-next-doc-id]
  (let [current-f (doc-freq-of-word doc-map current-doc-id)
        next-f (doc-freq-of-word doc-map next-doc-id)
        next-next-f (doc-freq-of-word doc-map next-next-doc-id)]
    {:freq current-f
     :act (act-value current-f next-f)
     :init (init-value current-f next-f next-next-f)}))


(defn add-acts
  "Returns a map of the words docs, with activations and inits added"
  [word id-chain]
  (into {}
        (for [i (range (count id-chain))]
          (let [current-doc-id (nth id-chain i)]
            {current-doc-id (value-map (:docs word)
                                       current-doc-id
                                       (get id-chain (inc i) 0)
                                       (get id-chain (inc (inc i)) 0))}))))


(defn omg-sort
  "create a map sorted in descending order, first by value, then by key. accessor-fn should
  be a function that returns the value from the map to be compared, given a value of a map
  (omg-sort {:a {:d 1} :b {:d 2}} :d) --> {:b {:d 2}, :a {:d 1}}
  from http://tllake.blogspot.com.tr/2010/10/sorting-clojure-maps-by-value.html"
  [super-map accessor-fn]
  (into (sorted-map-by
         (fn [key1 key2]
           (let [val1 (accessor-fn (super-map key1)) val2 (accessor-fn (super-map key2))]
             (cond
              (= val1 val2) (.compareTo key2 key1)
              (< val1 val2) 1
              :else -1))))
        super-map))

(defn add-all-acts
  "adds the act and inits of the entire chain"
  [chain]
  (into {}
        (pmap
         (fn [w]
           (let [acts (add-acts w (:ids chain))]
             {(:word w) {:docs (into (sorted-map) (sort acts))}}))
         (:words chain))))

(defn make-acted-chain
  "returns a chain with activation and init values from a path"
  [path]
  (let [doc-chain (doc-chain-from-dir path)
        chain (make-word-chain doc-chain)]
    {:doc-chain doc-chain
     :word-chain {:ids (:ids chain)
                  :words (add-all-acts chain)}}))


(defn make-complete-chain
  "returns acted chain with :doc-chain updated with all the data from word-chain "
  [acted-chain]
  (let [doc-chain (:doc-chain acted-chain)
        word-chain (:words (:word-chain acted-chain))]
    (assoc acted-chain :doc-chain
      (into {}
            (pmap
             (fn [doc]
               (let [word-set (keys (:word-map doc))]
                 {(:id doc)
                  (into {} (map
                            (fn [word]
                              {word
                               {:freq (:freq (get
                                              (:docs (get word-chain word))
                                              (:id doc)))
                                :act (:act (get
                                            (:docs (get word-chain word))
                                            (:id doc)))
                                :init (:init (get
                                              (:docs (get word-chain word))
                                              (:id doc)))}})
                            word-set))}))
             doc-chain)))))


(defn sorted-words-of-docs
  "returns, in :ids order, sorted words of all documents in doc-chain"
  [c kw sub-chain-sym]
  (let [ids (get-in c [:word-chain :ids])]
    (for [doc-id ids]
      (omg-sort (get-in c [sub-chain-sym doc-id]) kw))))


(defn update-chain-with-sorted-docs
  "returns the same chain, with docs sorted by id, and words in them sorted by kw"
  [chain sub-chain kw]
  (assoc chain sub-chain
    (into (sorted-map)
          (zipmap (get-in chain [:word-chain :ids])
                  (sorted-words-of-docs chain kw sub-chain)))))

(defn nth-word-of-doc
  "returns the nth word as a vector of [key value-map]"
  [doc-id n doc-chain]
  (-> doc-chain
      (get doc-id)
      seq
      (nth n)))


(defn generate-doc-word-pairs
  "generates doc-first, word-second order pairs"
  [chain sub-chain-sym]
  (for [i (range *max-pairs*)                         ;; FIRST PLACE TO LOOK WHEN OUT-OF-BOUNDS IS THROWN
        d (get-in chain [:word-chain :ids])]
    (flatten [d (nth-word-of-doc d i (sub-chain-sym chain))])))

(defn generate-in-doc-pairs
  "generates word-first doc-second order pairs"
  [chain sub-chain-sym]
  (for [d (get-in chain [:word-chain :ids])
        i (range (count (keys (get-in chain [sub-chain-sym d]))))]
    (flatten [d (nth-word-of-doc d i (sub-chain-sym chain))])))


(defn reach-kw
  [elem kw]
  (if (coll? elem)
    (-> elem
        last
        kw)
    elem))



;; KTOTAL ========================================

(defn add-limited-attrs
  "chain -> entire chain
  kw -> which attr to limit :init
  new-sym -> under which symbol will the new things added :limited-inits
  old-sym -> from which part of the chain will the new things be generated :doc-chain
  limit-num -> the limit to impose *kTotal*
  pair-generator -> generate-doc-word-pairs"
  [chain kw new-sym old-sym limit-num pair-generator]
  (assoc chain new-sym (->
                        (pair-generator chain old-sym)
                        (sum-to #(reach-kw % kw) limit-num))))


(defn organize-limited-attrs
  [chain sub-chain-sym]
  (assoc chain sub-chain-sym
    (loop [result (sorted-map) remains (sub-chain-sym chain)]
      (if (empty? remains)
        result
        (recur
         (let [current (first remains)]
           (assoc-in result
                     [(first current) (second current)]
                     (last current)))
         (rest remains))))))


;; KTRANS =============================

(defn filter-for-ktrans [c from kTrans]
  (->
   (from c)
   (->>
    (pmap val)
    (pmap (fn [docs] (into (sorted-map) (sum-to docs #(:act (val %)) kTrans)))))))


(defn add-both-limited [c from to kTrans]
  (assoc c to
    (let [filtered (filter-for-ktrans c from kTrans)]
      (into (sorted-map) (zipmap (get-in c [:word-chain :ids]) filtered)))))

(defn organize-both-limited [c from to kw]
  (assoc c to
    (let [sorted (->
                  c
                  from
                  vals
                  (->>
                   (pmap #(omg-sort % kw))))]
      (into (sorted-map) (zipmap (get-in c [:word-chain :ids]) sorted)))))

;;===============================================================

(defn chain
  "Generates the complete chain given path to xmls"
  [path kTrans kTotal]
  (-> path
      make-acted-chain
      make-complete-chain

      ;;       smoothness is ensured on non-trivial document chains, the possibility of any word
      ;;       having a total of 1 init is not worth the check. This entire document chain has a total
      ;;       sum of ints at 2.94, over 2863 words -> a word has 0.001 init on average

      ;;       kTotal ensured
      (update-chain-with-sorted-docs :doc-chain :freq)
      (add-limited-attrs :init :limited-inits :doc-chain kTotal generate-doc-word-pairs)
      (organize-limited-attrs :limited-inits)

      ;;       kTrans ensured
      (update-chain-with-sorted-docs :limited-inits :freq)
      (add-both-limited :limited-inits :both-limited kTrans)
      (organize-both-limited :both-limited :final-chain :freq)))

;; REPL

;; (def c (chain "resources"))
;; (log (:final-chain c) "final-chain-filtered-stops.txt")







