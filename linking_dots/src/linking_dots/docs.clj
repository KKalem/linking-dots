(ns linking-dots.docs
  (:gen-class)
  (:require [clojure.string :as strn]
            [linking-dots.nytimes :refer [read-nytimes]]
            [me.raynes.fs :as fs]
            [clojure.pprint :refer [pprint]]))

(defrecord doc [^String id ^String title word-map]) ;;word-map is a map that contains every word in the doc with their counts
;; document chains are simple vectors of doc-ids

(def ^:dynamic *stopwords* (->
                            (slurp "stopwords.txt")
                            (strn/split-lines)
                            set))

(defn tokenize
  "Return the corpus of the article as a list of words

  ex: (tokenize \" res/something.xml \" corpufy-nytimes) --> '(...)"
  [^String s]
  (filter (complement *stopwords*)
  (strn/split
   (strn/replace s
                 #"[\[\]!\"#$%&'()*+,./:\;<=>?@\^_`{|}~-]" " ")
   #"\s+")))


(defn- group-words
  "Returns the count of all words for the given word list

  ex: (count-words '(asd asd asd asd fgh fgh) asd) --> {asd [asd asd asd] ...}"
  [word-list]
  (group-by
   (fn [^String w]  w)
   word-list))

(defn map-of-words
  "Helper function that returns a map with {word freq word freq ...}"
  [^String corpus]
  (let [tokenized (tokenize corpus)
        grouped-words (group-words tokenized)
        total-word-count (count tokenized)]
    (into {}
          (map
           (fn [me] {(key me) {:freq (/ (count (val me)) total-word-count)}}) ;; TESSSSSST {:freq ekledim}
           grouped-words))))

(defn- id-from-path
  "filename will be used as id"
  [path]
  (fs/name path))

;============================================================================================

(defn doc-from-file
  "id-fn is a function that returns an ID for the given path
  reader-fn is a function that returns a map {:title title :corpus corps}

  Returns a doc record for the given path"
  [^String path id-fn reader-fn]
  (let [id (id-fn path)
        r (reader-fn path)
        title (:title r)
        word-map (map-of-words (:corpus r))]
    {:id id
     :title title
     :word-map word-map}))


(defn doc-from-nytimes
  "Uses default nytimes functions for ease of use"
  [path]
  (doc-from-file path id-from-path read-nytimes))


(defn word-count-from-doc [doc ^String word]
  "returns the freq of word in the doc"
  (:freq (get (:word-map doc) word)))


(defn doc-chain-from-dir
  "Returns a vector of docs from the given directory at path.

  ex: (doc-chain-from-dir \"resources\") ==> [ <doc1> , <doc2> ...] "
  ([^String path]
   (doc-chain-from-dir path id-from-path read-nytimes))

  ([^String path id-fn reader-fn]
   (let [file-names (map fs/base-name (fs/list-dir path))
         file-paths (map #(str path '/ %) file-names)]
     (into []
           (map #(doc-from-file % id-fn reader-fn)
                file-paths)))))

