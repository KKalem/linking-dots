(ns linking-dots.word-set
  (:gen-class)
  (:require [linking-dots.docs :refer [doc-chain-from-dir]]
            [criterium.core :as crit]
            [linking-dots.util :refer [log]]))

;; chain = { :docs [doc-id id id id]
;;           { :words
;;             #{
;;               word
;;               word
;;             }
;;           }
;;         }

(defn word-freq
  "Returns the freq of given word in the doc, 0 if not found"
  [doc ^String word]
  (get (get (:word-map doc) word) :freq 0))


(defn word-set-of-doc-chain
  "doc-chain is a vector of docs.doc s"
  [doc-chain]
  (loop [docs-left doc-chain word-set #{}]
    (if docs-left
      (recur (next docs-left) (into word-set (keys (:word-map (first docs-left)))))
      word-set)))

(defn id-chain
  "returns the ids of given chain in order in a vector"
  [doc-chain]
  (into [] (map #(:id %) doc-chain)))

(defn docs-with-word
  "returns the docs that contain the given word as ( {id freq} )"
  [doc-chain word]
  (into {}
        (filter #(not (nil? %))
                (for [d doc-chain]
                  (let [freq (word-freq d word)]
                    (when (not= 0 freq) {(:id d) {:freq freq}}))))))

;; ==================================================================================================================

(defn make-word-chain
  "Returns a {:ids [id-chain] :words {word {doc-id freq} } }"
  [doc-chain]
  (let [ws (word-set-of-doc-chain doc-chain)]
    {:ids (id-chain doc-chain)
     :words
     (map (fn [w]
            {:word w :docs (docs-with-word doc-chain w)})
          ws)}))
