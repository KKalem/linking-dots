(ns linking-dots.nytimes
  (:gen-class)
  (:require [clojure.string :as strn]
            [clojure.data.xml :as xml]
            [linking-dots.util :refer [log]]))





(defn nytimes-corpufy-txt [txt]
  "Return the corpus of the article from the given raw text of an xml"
        (strn/lower-case (reduce str ""
                           (reduce concat '()
                                   (map :content
                                        (filter #(= :p (:tag %))
                                                (:content (second (:content (second (:content (second (:content txt)))))))))))))




(defn nytimes-doc-title-txt [txt]
  "Return the title of the doc at the given raw text of an xml"
  (strn/lower-case  (first (:content (first (:content (first (:content txt))))))))


(def read-nytimes (memoize (fn [path]
  "Returns the title and the corpus of a nytimes article given in path. {:title title-of-article :corpus corpus}"
  (let [text (xml/parse-str (slurp path) :support-dtd false)]
    {:title (nytimes-doc-title-txt text) :corpus (nytimes-corpufy-txt text)}))))
