(ns linking-dots.util
  (:gen-class)
  (:require [clojure.pprint :refer [pprint]]))

;; util nytimes docs word-set lpfying di-graph minedge

(defn log
  ([fnk]
   (let [res fnk]
     (do
       (spit "log.txt" (with-out-str (pprint res)) :append :true)
       res)))
  ([fnk ^String path]
   (let [res fnk]
     (do
       (spit path (with-out-str (pprint res)) :append :true)
       res))))

(defn normalize
  "returns a list, normalized"
  [v]
  (let [s (reduce + v)]
    (if (= 0 s)
      (repeat (count v) 0)
      (map #(double (/ % s)) v))))


(defn sum-to
  "returns the sublist, that sums upto given target, accessed by the access-fn"
  ([coll target]
   (sum-to coll identity target))
  ([coll access-fn target]
  (loop [current-target target remains coll result []]
    (if (or (< current-target 0) (empty? remains))
      (butlast result)
      (recur (- current-target (access-fn (first remains))) (rest remains) (conj result (first remains)))))))
