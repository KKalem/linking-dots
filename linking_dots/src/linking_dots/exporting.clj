(ns linking-dots.exporting
  (:require
   [clojure.pprint :refer [pprint]]
   [linking-dots.util :as l]
   [linking-dots.lpfying :refer [chain]]
   [clojure-csv.core :as csv]
   [linking-dots.minedge :refer [grid]]))


(use '(incanter core stats charts io))


(defn value-vec-of-doc
  "returns a list of values for the given doc and keyweord"
  [doc kw]
  (-> doc
      vals
      (#(map kw %))
      (#(into [] %))))

(defn value-vecs-of-doc-chain
  [doc-chain kw]
  (map #(value-vec-of-doc % kw) (vals doc-chain)))


(defn nth-doc-values
  [complete-chain n kw]
  (->
   complete-chain
   :doc-chain
   (value-vecs-of-doc-chain kw)
   (nth n)))

(defn sorted-nth-doc-values
  [complete-chain n kw]
  (->
   (nth-doc-values complete-chain n kw)
   sort
   reverse))


(defn plot-docs
  [complete-chain kw show-points sort-by-kw max-range]
  (let [docs (-> complete-chain
                 :word-chain
                 :ids)
        plot (xy-plot nil nil :legend true :x-label "words" :y-label (name kw))
        value-func (if sort-by-kw sorted-nth-doc-values nth-doc-values)]
    (loop [i (dec (count docs)) new-plot plot]
      (if (zero? (inc i))
        new-plot
        (recur (dec i) (add-lines new-plot (range 0 max-range) (value-func complete-chain i kw) :points show-points :series-label (docs i)))))))


(defn word-set-of-chain
  [complete-chain]
  (-> complete-chain
      :word-chain
      :words
      keys))


(defn flat-value-of-word
  "returns a vector of values in id-order, of given word and freq, from the complete chain"
  [complete-chain word kw]
  (->
   complete-chain
   :word-chain
   :words
   (get word)
   :docs
   vals
   (->>
    (map kw)
    (map float)
    (map str))))


(defn flatten-chain
  [complete-chain kw]
  (->
   complete-chain
   word-set-of-chain
   (->>
    (map (fn [word] [word (flat-value-of-word complete-chain word kw)])))))

(defn filter-flat-chain
  [flat-chain]
  (filter
   (fn [w] (not-every? #(= "0.0" %) (second w)))
   flat-chain))

(defn spit-csv
  [flat-chain path]
  (map
   (fn [w]
     (spit
      path
      (csv/write-csv [(flatten w)])
      :append :true))
   flat-chain))

(defn add-title-to-flat-chain
  [flat-chain complete-chain]
  (let [ids (:ids (:word-chain complete-chain))]
    (conj flat-chain (into [] (flatten ["word" ids])))))

(defn spit-flat-csv
  [complete-chain kw]
  (->
   complete-chain
   (flatten-chain kw)
   (add-title-to-flat-chain complete-chain)
   (spit-csv (str "complete-chain--" (name kw) ".csv"))))

;; REPL

;; (save
;;  (plot-docs (chain "resources" 0.2 0.3) :init true true 50)
;;  "sorted-inits.png" :width 1000 :height 1000)


;; (def grd (doall (grid 0 1 0 1 0.05)))
;; grd


;; (->>
;;  grd
;;  (map (fn [row] (map str row)))
;;  csv/write-csv
;;  (spit "1-1-0.05.csv"))









