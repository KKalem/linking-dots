(ns linking-dots.minedge
  (:gen-class)
  (:require
   [clojure.pprint :refer [pprint]]
   [linking-dots.util :refer [log]]
   [linking-dots.lpfying :refer [chain *kTrans* *kTotal* omg-sort]]
   [linking-dots.di-graph :refer [graph influence]]
   [clojure-csv.core :as csv]))


;; (use '(incanter core stats charts io))
;; (def c (chain "resources"))
;; (def g (graph c :freq))
;; (def final-chain (:final-chain c))
;; (def ids (get-in c [:word-chain :ids]))


(defn minedge [current-doc-id next-doc-id g final-chain]
  (let [word-map (get final-chain current-doc-id)
        words (keys word-map)]
    (->> words
         (pmap (fn [word] (* (influence word current-doc-id next-doc-id g) (get-in word-map [word :act]))))
         (reduce +))))


(defn all-edges [g final-chain ids]
  (omg-sort
   (zipmap ids
           (for [i (range (dec (count ids)))]
             (let [current-doc-id (nth ids i)
                   next-doc-id (nth ids (inc i))]
               (minedge current-doc-id next-doc-id g final-chain)))) identity))


(defn max-min-edge [kTrans kTotal path]
  (let [c (chain path kTrans kTotal)
        g (graph c :freq)
        final-chain (:final-chain c)
        ids (get-in c [:word-chain :ids])]
    (first
     (vals (all-edges g final-chain ids)))))



(defn grid [kTrans-from kTrans-to kTotal-from kTotal-to step]
  (for [s (range kTrans-from kTrans-to step) l (range kTotal-from kTotal-to step)]
    [s l (max-min-edge s l "resources")]))



(defn logging-grid [kTrans-from kTrans-to kTotal-from kTotal-to step path-from path-to]
  (for [s (range kTrans-from kTrans-to step) l (range kTotal-from kTotal-to step)]
    (->> [[s l (max-min-edge s l path-from)]]
         (map (fn [row] (map str row)))
         csv/write-csv
         (#(spit path-to % :append :true)))))


;; (def res-unmixed (max-min-edge 0.2 0.5 "resources")) ;0.0536
;; (reduce + res-unmixed) ; 0.158

;; (def res-mixed (max-min-edge 0.2 0.5 "mixed-chain")) ;0.0620
;; (reduce + res-mixed) ; 0.0137

;; (def res-mixed2 (max-min-edge 0.2 0.5 "mixed-chain2")) ;0.0818
;; (reduce + res-mixed2) ; 0.167


