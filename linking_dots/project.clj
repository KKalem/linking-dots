(defproject linking_dots "0.1"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [

                 [org.clojure/clojure "1.6.0"]
                 [org.clojure/data.xml "0.0.8"] ;xml parsing
                 ;[clucy "0.4.0"] ;luceene indexing
                 ;[clocop "0.2.0"] ;linear program solver
                 [me.raynes/fs "1.4.6"] ;file system utils
                 [criterium "0.4.3"] ;testing eval times
                 [incanter "1.5.6"] ;all the graphs!
                 [net.mikera/core.matrix "0.34.0"] ;needed for incanter? maybe
                 [clojure-csv/clojure-csv "2.0.1"] ;csv support, excel has nice visualizers
                 [ubergraph "0.1.2"] ;moar graphs! (non-visual this time)
                 ]
  :main ^:skip-aot linking-dots.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  )
