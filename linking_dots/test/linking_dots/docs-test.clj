(ns linking-dots.docs-test
  (:require [clojure.test :refer :all]
            [linking-dots.nytimes :refer :all]
            [linking-dots.docs :refer :all]
            [clojure.string :as strn]
            [clojure.data.xml :as xml]
            [criterium.core :as crit]))

(def test-path "resources/test-doc.xml")
(def test-text (xml/parse-str (slurp test-path) :support-dtd false))
(def test-corpus "negotiations between monica s. lewinsky's lawyer and kenneth w. starr, the independent counsel, have stalled, lawyers say, partly because ms. lewinsky is not offering prosecutors  as much detail about her conversations with president clinton as she reportedly disclosed to a friend on secretly recorded tapes.")
(def test-title "test-title")
(def test-corpus3 "a a a a a a a a a a b b b a a a c c b b b b c c c c d d c d d d")

(deftest test-doc-from-nytimes
  (testing "extraction of everything from a nytimes doc"
    (is
     (=
      (:title (doc-from-nytimes test-path))
      (:title (->doc "test-doc.xml" "test-title" {}))))))


;; (time (doc-chain-from-dir "resources")) ; max 1500ms for 9 docs -> 52 ms with new stuff

;; (run-tests)


;; (def crit-corpus (tokenize "resources/1_0991630.xml"))
;; (crit/quick-bench (map-of-words crit-corpus)) ; 19~us mean ---removed!
;; (crit/quick-bench (alt-map-of-words crit-corpus)) ; 15~us mean ---currently in use as map-of-words

;; (crit/quick-bench (doc-chain-from-dir "resources")) ; 43ms~ for a 9 doc chain. above optimization reduced estimated counting of entire 85k chain lattice form 4 days to 10 hours. win? win.

