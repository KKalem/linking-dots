(ns linking-dots.extract-test
  (:require [clojure.test :refer :all]
            [linking-dots.extract :refer :all]))

(def sample-text "test!@# test7594 /.,test")

(deftest test-tokenize
  (testing "Tokenizer leaves only lowercase words"
    (is
     (= ["test" "test7594" "test"] (tokenize sample-text)))))

(deftest test-count-word
  (testing "Count words returns correctly"
    (is
     (= {:word "test", :cnt 2} (count-word (tokenize sample-text) "test")))))

;;TODO test map-words after converion


(run-tests)
