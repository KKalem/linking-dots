(ns linking-dots.nytimes-test
  (:require [clojure.test :refer :all]
            [linking-dots.nytimes :refer :all]
            [clojure.string :as strn]
            [clojure.data.xml :as xml]))

(def test-path "resources/test-doc.xml")
(def test-text (xml/parse-str (slurp test-path) :support-dtd false))
(def test-corpus "negotiations between monica s. lewinsky's lawyer and kenneth w. starr, the independent counsel, have stalled, lawyers say, partly because ms. lewinsky is not offering prosecutors  as much detail about her conversations with president clinton as she reportedly disclosed to a friend on secretly recorded tapes.")
(def test-title "test-title")


(deftest test-corpufy
  (testing "NYTimes corpufier returning a single string"
    (is
     (= test-corpus (nytimes-corpufy-txt test-text)))))

(deftest test-doc-title
  (testing "NYTimes doc title correct"
    (is
     (= test-title (nytimes-doc-title-txt test-text)))))

(deftest test-read-nytimes
  (testing "Resulting document info correct?"
    (is
     (=
      {:title test-title :corpus test-text}
      (read-nytimes test-path)))))   ;; this fails for some reason, deferring to later date.

(run-tests)


