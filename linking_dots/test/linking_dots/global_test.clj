(ns linking-dots.global_test
  (:require [clojure.test :refer :all]
            [linking-dots.global :refer :all]))

(deftest test-make-doc
  (testing "Making new document map"
    (is
     (= {:title "test" :cnt 3 :act 0.2 :init 0.6} (make-doc "test" 3 0.2 0.6)))))

(deftest test-make-word
  (testing "Making new word map"
    (is
     (= {:word "test" :docs '()} (make-word "test")))))


(deftest test-add-doc-to-word
  (testing "Adding a new document to a word"
    (let [d (make-doc "test-title" 3 0.2 0.6)
          w (make-word "test-word")
          uw (add-doc-to-word w d)]
      (is
       (= uw {:word "test-word", :docs '({:title "test-title", :cnt 3, :act 0.2, :init 0.6})})))))

(deftest test-doc-ops
  (testing "Adding act and init to a doc"
    (is
     (= {:title "test", :cnt 3, :act 10, :init 1.3}
        (-> (make-doc "test" 3 -1 -1)
            (add-act-to-doc 10)
            (add-init-to-doc 1.3))))))

(run-tests)
