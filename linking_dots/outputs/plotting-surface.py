import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

data = np.genfromtxt('ZOMG.csv', delimiter=',')

start = 0.5
end = 1.5
step = 0.01

grid_size = int(1+((end-start)/step))

trans = np.linspace(start,end,grid_size)
total = np.linspace(start,end,grid_size)
te0, te1 = np.meshgrid(trans,total)
minedge = np.zeros((grid_size,grid_size))

for i in range(grid_size):
    for j in range(grid_size):
        minedge[i][j] = data[(i+11*j)][2]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(te0, te1, minedge, cmap=cm.jet, linewidth=0, antialiased=False, alpha=1, rstride=1, cstride=1)
ax.set_xlabel('kTrans')
ax.set_ylabel('kTotal')
ax.set_zlabel('minEdge')


